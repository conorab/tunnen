# Tunnen

Establishes an SSH port forward, verifies the ports are forwarding, then starts the given application. If the given application or the required ports cease to respond, the session and application are closed. 