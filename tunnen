#!/bin/bash

# tunnen - v1 - Copyright (C) 2020 Conor Andrew Buckley

#This program is free software; you can redistribute it and/or modify
#it under the terms of the GNU General Public License v2.0 as published by
#the Free Software Foundation.

#This program is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License v2.0 for more details.

#You should have received a copy of the GNU General Public License along
#with this program; if not, write to the Free Software Foundation, Inc.,
#51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.# Exit Codes

	# 1 - Timed out while trying to establish the S.S.H. connection/tunnel.
	# 2 - The remote host to forward ports through (not the target of the port to forward) has not been specified.
	# 3 - The remote host whose port we are forwarding to has NOT been specified.
	# 4 - The local port which we are port forwarding to has NOT been specified.
	# 5 - The port on the remote host which we are forwarding has NOT been specified.

# Arguments

	# 1 - The remote S.S.H. host to connect to which will enable the S.S.H. port forwarding.
	#	- If you wish to set things like the remote user, public key, etc, do so in the S.S.H. client config.
	#	- You can use an alias to the host if you want to use a different user specifically for this task.
	# 2 - The I.P./host on the local machine to bind to when listening on the forwarded port
	# 3 - The host whose port will be forwarded via the remote S.S.H. host to us.
	# 4 - The port on the local host which S.S.H. will listen and forward connections with.
	# 5 - The port on the remote host (not necessarily the S.S.H. host) which will be forwarded via the S.S.H. host to the local port.
	# 6 - The command to run on the local machine once the tunnel is active, and to kill once the tunnel dies.

# The remote S.S.H. host to connect to which will enable the S.S.H. port forwarding.

	unset SSHRemote_Host
	SSHRemote_Host="$1"

# The I.P./host on the local machine to bind to when listening on the forwarded port.

	unset SSHLocal_ForwardHost
	SSHLocal_ForwardHost="$2"

# The host whose port will be forwarded via the remote S.S.H. host to us.

	unset SSHRemote_ForwardHost
	SSHRemote_ForwardHost="$3"

# The port on the local host which S.S.H. will listen and forward connections with.

	unset SSHLocal_ForwardPort
	SSHLocal_ForwardPort="$4"

# The port on the remote host (not necessarily the S.S.H. host) which will be forwarded via the S.S.H. host to the local port.

	unset SSHRemote_ForwardPort
	SSHRemote_ForwardPort="$5"

# Verify that the required arguments have been provided.

	if [ ! "$SSHRemote_Host" ]

		then
			echo 'tunnen: The remote host to forward ports through (not the target of the port to forward) has NOT been specified.' >&2
			exit 2
	fi

	if [ ! "$SSHRemote_ForwardHost" ]

		then
			echo 'tunnen: The remote host whose port we are forwarding to has NOT been specified.' >&2
			exit 3
	fi

	if [ ! "$SSHLocal_ForwardPort" ]

		then
			echo 'tunnen: The local port which we are port forwarding to has NOT been specified.' >&2
			exit 4
	fi

	if [ ! "$SSHRemote_ForwardPort" ]

		then
			echo 'tunnen: The port on the remote host which we are forwarding has NOT been specified.' >&2
			exit 5
	fi

# Establish the S.S.H. connection and forward the given ports.
# We also obtain the P.I.D. of the S.S.H. process we are using for forwarding so that we can monitor this process.
# We are not waiting for the S.S.H. connection to be established here, nor are we checking if the program exists; we do that elsewhere using the P.I.D. we obtained.

	unset TunnelPID
	ssh "$SSHRemote_Host" -N -L "$SSHLocal_ForwardHost"\:"$SSHLocal_ForwardPort"\:"$SSHRemote_ForwardHost"\:"$SSHRemote_ForwardPort" &> /dev/null &
	TunnelPID="$!"

# The amount of attempts which are made to establish a T.C.P. connection to the local port (which should be forwarded to the remote host via the remote S.S.H. host) with 1 second of padding per attempt before we give up and assume that the port forwarding failed.
# On each attempt we are also checking if the P.I.D. of the S.S.H. connection used for port forwarding is still running.
# If it ceases to run, we treat this as a failure.

	echo -n 'Waiting for tunnel to open: '
	unset FailCount
	FailCount=0
	while ! bash -c 'echo </dev/tcp/127.0.0.1/'"$SSHLocal_ForwardPort" &> /dev/null && [ "$FailCount" -lt '30' ] && kill -0 "$TunnelPID" &> /dev/null

		do
			echo -n \.
			((FailCount++))
			sleep 1
	done

	if [ "$FailCount" == '30' ] || ! kill -0 "$TunnelPID" &> /dev/null

		then
			echo ' DIED/TIMED OUT!'
			kill "$TunnelPID" &> /dev/null
			wait "$TunnelPID"
			echo 'tunnen: Timed out while trying to establish the S.S.H. connection/tunnel.' >&2
			exit 1

		else
			echo ' UP!'
	fi
	unset FailCount

####
# Run the application which is dependant on the tunnel and obtain it's P.I.D.
# We check if the application is still running at a later step.

	unset DepPID
	sh -c "$6" &
	DepPID="$!"

# Once a second, check if either the P.I.D. of the S.S.H. tunnel or the P.I.D. of the dependant application ceases to run.
# If so, send kill signals to both P.I.D.s (ignoring errors) and wait for both P.I.D.s to stop running, before exitting the script.
# We are not checking if the tunnel P.I.D. exits first and if so, return an error at this point since I'm not sure what the best way is to do so in a reliable fashion; the S.S.H. P.I.D. could close but before we have a chance to check if the dependant application is still up, that closes, meaning that while the S.S.H. P.I.D. closed first and caused the dependant application to close, we couldn't detect it quickly enough and so we give the impression that there was nothing wrong with the tunnel.

# NOTE: We exit with an exit code of zero here in case 'kill' or 'wait' return non-zero exit codes and give the impression that the script has encountered an error when it hasn't.

	while kill -0 "$DepPID" &> /dev/null && kill -0 "$TunnelPID" &> /dev/null

		do
			sleep 1
	done

	kill "$DepPID" "$TunnelPID" &> /dev/null
	wait "$DepPID" "$TunnelPID" &> /dev/null

	exit 0

